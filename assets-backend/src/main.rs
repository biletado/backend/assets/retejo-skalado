use crate::api::route::init_router;
use tracing::debug;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt, EnvFilter};

mod api;
mod database;

#[tokio::main]
async fn main() {
    dotenvy::dotenv().ok();

    tracing_subscriber::registry()
        .with(
            EnvFilter::try_from_env("RUST_LOG_LEVEL")
                .unwrap_or_else(|_| "biletado-backend=debug".into()),
        )
        // TDB: Decide between json and pretty as logging style and format
        .with(tracing_subscriber::fmt::layer().json())
        .init();

    let app = init_router().await;

    let addr = "0.0.0.0:3000";
    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();
    debug!("listening on {}", addr);
    axum::serve(listener, app).await.unwrap();
}
