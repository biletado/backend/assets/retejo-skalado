use diesel::{AsChangeset, Identifiable, Insertable, Queryable};
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;
use uuid::Uuid;

use crate::database::schema::{buildings, rooms, storeys};

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable, Identifiable)]
/// Building type that is identified by an UUID and consists of name, address and deletion time.
pub struct Building {
    pub id: Uuid,
    pub name: String,

    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub address: Option<String>,

    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(with = "time::serde::rfc3339::option")]
    pub deleted_at: Option<OffsetDateTime>,
}

#[derive(Debug, Clone, Deserialize, AsChangeset, Insertable)]
#[diesel(treat_none_as_null = true)]
#[diesel(table_name = buildings)]
/// Building data transfer object with optional UUID and deletion time that may be passed as part of a POST or PUT HTTP request.
pub struct BuildingDTO {
    #[serde(default = "Uuid::new_v4")]
    pub id: Uuid,
    pub name: String,
    pub address: String,

    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(with = "time::serde::rfc3339::option")]
    pub deleted_at: Option<OffsetDateTime>,
}

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable, Identifiable)]
/// Storey type that is identified by an UUID and consists of an associated building UUID, name and deletion time.
pub struct Storey {
    pub id: Uuid,
    pub name: String,
    pub building_id: Uuid,

    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(with = "time::serde::rfc3339::option")]
    pub deleted_at: Option<OffsetDateTime>,
}

#[derive(Debug, Clone, Deserialize, AsChangeset, Insertable)]
#[diesel(treat_none_as_null = true)]
#[diesel(table_name = storeys)]
/// Storey data transfer object with optional UUID and deletion time that may be passed as part of a POST or PUT HTTP request.
pub struct StoreyDTO {
    #[serde(default = "Uuid::new_v4")]
    pub id: Uuid,
    pub name: String,
    pub building_id: Uuid,

    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(with = "time::serde::rfc3339::option")]
    pub deleted_at: Option<OffsetDateTime>,
}

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable, Identifiable)]
/// Room type that is identified by an UUID and consists of an associated storey UUID, name and deletion time.
pub struct Room {
    pub id: Uuid,
    pub name: String,
    pub storey_id: Uuid,

    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(with = "time::serde::rfc3339::option")]
    pub deleted_at: Option<OffsetDateTime>,
}

#[derive(Debug, Clone, Deserialize, AsChangeset, Insertable)]
#[diesel(treat_none_as_null = true)]
#[diesel(table_name = rooms)]
/// Room data transfer object with optional UUID and deletion time that may be passed as part of a POST or PUT HTTP request.
pub struct RoomDTO {
    #[serde(default = "Uuid::new_v4")]
    pub id: Uuid,
    pub name: String,
    pub storey_id: Uuid,

    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(with = "time::serde::rfc3339::option")]
    pub deleted_at: Option<OffsetDateTime>,
}
