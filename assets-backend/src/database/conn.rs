use deadpool_diesel::postgres::{Manager, Pool};
use deadpool_diesel::Runtime::Tokio1;
use std::env;
use tracing::{debug, error};

pub fn init_connection_pool() -> Pool {
    let db_user = env::var("POSTGRES_ASSETS_USER")
        .expect("POSTGRES_ASSETS_USER environment variable not set");
    let db_pass = env::var("POSTGRES_ASSETS_PASSWORD")
        .expect("POSTGRES_ASSETS_PASSWORD environment variable not set");
    let db_name = env::var("POSTGRES_ASSETS_DBNAME")
        .expect("POSTGRES_ASSETS_DBNAME environment variable not set");
    let db_host = env::var("POSTGRES_ASSETS_HOST")
        .expect("POSTGRES_ASSETS_HOST environment variable not set");
    let db_port = env::var("POSTGRES_ASSETS_PORT")
        .expect("POSTGRES_ASSETS_PORT environment variable not set");

    let db_url = format!(
        "postgres://{}:{}@{}:{}/{}",
        db_user, db_pass, db_host, db_port, db_name
    );

    debug!("attempting to create database connection cool...");
    let manager = Manager::new(db_url, Tokio1);
    match Pool::builder(manager).build() {
        Ok(pool) => {
            debug!("successfully created database connection pool!");
            pool
        }
        Err(e) => {
            error!("failed to create database connection pool: {}", e);
            std::process::exit(1);
        }
    }
}
