use diesel::table;

table! {
    buildings (id) {
        id -> Uuid,
        name -> Text,
        address -> Nullable<Text>,
        deleted_at -> Nullable<Timestamptz>
    }
}

table! {
    storeys (id) {
        id -> Uuid,
        name -> Text,
        building_id -> Uuid,
        deleted_at -> Nullable<Timestamptz>
    }
}

table! {
     rooms (id) {
        id -> Uuid,
        name -> Text,
        storey_id -> Uuid,
        deleted_at -> Nullable<Timestamptz>
    }
}
