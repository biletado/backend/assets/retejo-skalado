use deadpool_diesel::postgres::Pool;
use diesel::{dsl::exists, insert_into, select, update, ExpressionMethods, QueryDsl, RunQueryDsl};
use time::OffsetDateTime;
use tracing::debug;
use uuid::Uuid;

use crate::{
    database::model::{Storey, StoreyDTO},
    database::schema::storeys::{building_id, deleted_at, dsl::storeys, id},
};

/// Check if a building has associated storeys.
/// Returns true if a building has associated storeys, otherwise false.
pub async fn has_storeys(pool: Pool, building: Uuid) -> bool {
    let conn = pool.get().await.unwrap();
    conn.interact(move |conn| {
        select(exists(
            storeys
                .filter(building_id.eq(building))
                .filter(deleted_at.is_null()),
        ))
        .get_result(conn)
        .unwrap_or(false)
    })
    .await
    .unwrap()
}

/// Get all existing storeys of a building and depending on the include_deleted argument also the soft deleted ones.
/// Returns a vector of storeys.
pub async fn get_storeys_by_building(
    pool: Pool,
    building: Uuid,
    include_deleted: bool,
) -> Vec<Storey> {
    let conn = pool.get().await.unwrap();

    conn.interact(move |conn| {
        if include_deleted {
            storeys
                .filter(building_id.eq(building))
                .load::<Storey>(conn)
                .unwrap()
        } else {
            storeys
                .filter(building_id.eq(building))
                .filter(deleted_at.is_null())
                .load::<Storey>(conn)
                .unwrap()
        }
    })
    .await
    .unwrap()
}

/// Find a storey by UUID.
/// Returns a storey with the corresponding UUID or `None` if the UUID does not exist in the table.
pub async fn find_storey_by_id(pool: Pool, storey_id: Uuid) -> Option<Storey> {
    let conn = pool.get().await.unwrap();
    conn.interact(move |conn| storeys.find(storey_id).first::<Storey>(conn).ok())
        .await
        .unwrap()
}

/// Creates a new storey using the storey data transfer object passed as argument.
/// Returns the created storey.
pub async fn create_storey(pool: Pool, storey: StoreyDTO) -> Storey {
    let conn = pool.get().await.unwrap();

    conn.interact(|conn| {
        insert_into(storeys)
            .values(storey)
            .get_result::<Storey>(conn)
            .unwrap()
    })
    .await
    .unwrap()
}

/// Pass a storey data transfer object as argument.
/// If the UUID already exists, update the storey with the new name, building and deletion time.
/// If the UUID does not exist, create a new storey with that UUID.
/// Returns the created or updated storey
pub async fn update_storey(pool: Pool, storey: StoreyDTO) -> Storey {
    let conn = pool.get().await.unwrap();
    conn.interact(move |conn| {
        if let Ok(existing_storey) = &storeys.find(storey.id).get_result::<Storey>(conn) {
            debug!("existing storey: {:#?}", existing_storey);
            debug!("storey dto: {:#?}", storey);
            update(existing_storey)
                .set(&storey)
                .get_result::<Storey>(conn)
                .unwrap()
        } else {
            // could be replaced with unwrap_or(create_storey(pool, storey).await), but results
            // in closure returning a async block and requires move of ownership which leads to
            // problems with the lifetime of the connection object's mutable borrow
            insert_into(storeys)
                .values(storey)
                .get_result::<Storey>(conn)
                .unwrap()
        }
    })
    .await
    .unwrap()
}

/// Soft delete the storey with the provided UUID.
/// Return true if deletion was successful and false if the UUID was not found.
pub async fn delete_storey_by_id(pool: Pool, storey_id: Uuid) -> bool {
    let conn = pool.get().await.unwrap();

    conn.interact(move |conn| {
        let not_deleted = select(exists(
            storeys
                .filter(id.eq(storey_id))
                .filter(deleted_at.is_null()),
        ))
        .get_result(conn)
        .unwrap_or(false);

        if not_deleted {
            update(storeys.filter(id.eq(storey_id)))
                .set(
                    deleted_at.eq(OffsetDateTime::now_local().unwrap_or(OffsetDateTime::now_utc())),
                )
                .execute(conn)
                .unwrap();
            return true;
        }
        return false;
    })
    .await
    .unwrap()
}
