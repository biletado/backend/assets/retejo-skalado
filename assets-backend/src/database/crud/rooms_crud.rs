use deadpool_diesel::postgres::Pool;
use diesel::{dsl::exists, insert_into, select, update, ExpressionMethods, QueryDsl, RunQueryDsl};
use time::OffsetDateTime;
use tracing::debug;
use uuid::Uuid;

use crate::{
    database::model::{Room, RoomDTO},
    database::schema::rooms::{deleted_at, dsl::rooms, id, storey_id},
};

/// Check if a storey has associated rooms.
/// Returns true if a storey has associated rooms, otherwise false.
pub async fn has_rooms(pool: Pool, storey: Uuid) -> bool {
    let conn = pool.get().await.unwrap();

    conn.interact(move |conn| {
        select(exists(
            rooms
                .filter(storey_id.eq(storey))
                .filter(deleted_at.is_null()),
        ))
        .get_result(conn)
        .unwrap_or(false)
    })
    .await
    .unwrap()
}

/// Get all existing rooms of a storey and depending on the include_deleted argument also the soft deleted ones.
/// Returns a vector of rooms.
pub async fn get_rooms_by_storey(pool: Pool, storey: Uuid, include_deleted: bool) -> Vec<Room> {
    let conn = pool.get().await.unwrap();

    conn.interact(move |conn| {
        if include_deleted {
            rooms
                .filter(storey_id.eq(storey))
                .load::<Room>(conn)
                .unwrap()
        } else {
            rooms
                .filter(deleted_at.is_null())
                .filter(storey_id.eq(storey))
                .load::<Room>(conn)
                .unwrap()
        }
    })
    .await
    .unwrap()
}

/// Find a room by UUID.
/// Returns a room with the corresponding UUID or `None` if the UUID does not exist in the table.
pub async fn find_room_by_id(pool: Pool, room_id: Uuid) -> Option<Room> {
    let conn = pool.get().await.unwrap();
    conn.interact(move |conn| rooms.find(room_id).first::<Room>(conn).ok())
        .await
        .unwrap()
}

/// Creates a new room using the room data transfer object passed as argument.
/// Returns the created room.
pub async fn create_room(pool: Pool, room: RoomDTO) -> Room {
    let conn = pool.get().await.unwrap();

    conn.interact(|conn| {
        insert_into(rooms)
            .values(room)
            .get_result::<Room>(conn)
            .unwrap()
    })
    .await
    .unwrap()
}

/// Pass a room data transfer object as argument.
/// If the UUID already exists, update the room with the new name, storey and deletion time.
/// If the UUID does not exist, create a new room with that UUID.
/// Returns the created or updated room
pub async fn update_room(pool: Pool, room: RoomDTO) -> Room {
    let conn = pool.get().await.unwrap();
    conn.interact(move |conn| {
        if let Ok(existing_room) = &rooms.find(room.id).get_result::<Room>(conn) {
            debug!("existing room: {:#?}", room);
            debug!("room dto: {:#?}", room);
            update(existing_room)
                .set(&room)
                .get_result::<Room>(conn)
                .unwrap()
        } else {
            // could be replaced with unwrap_or(create_room(pool, room).await), but results
            // in closure returning a async block and requires move of ownership which leads to
            // problems with the lifetime of the connection object's mutable borrow
            insert_into(rooms)
                .values(room)
                .get_result::<Room>(conn)
                .unwrap()
        }
    })
    .await
    .unwrap()
}

/// Soft delete the room with the provided UUID.
/// Return true if deletion was successful and false if the UUID was not found.
pub async fn delete_room_by_id(pool: Pool, room_id: Uuid) -> bool {
    let conn = pool.get().await.unwrap();

    conn.interact(move |conn| {
        let not_deleted = select(exists(
            rooms.filter(id.eq(room_id)).filter(deleted_at.is_null()),
        ))
        .get_result(conn)
        .unwrap_or(false);

        if not_deleted {
            update(rooms.filter(id.eq(room_id)))
                .set(
                    deleted_at.eq(OffsetDateTime::now_local().unwrap_or(OffsetDateTime::now_utc())),
                )
                .execute(conn)
                .unwrap();
            return true;
        }
        return false;
    })
    .await
    .unwrap()
}
