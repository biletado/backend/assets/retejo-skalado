use deadpool_diesel::postgres::Pool;
use diesel::{dsl::exists, insert_into, select, update, ExpressionMethods, QueryDsl, RunQueryDsl};
use time::OffsetDateTime;
use tracing::debug;
use uuid::Uuid;

use crate::{
    database::model::{Building, BuildingDTO},
    database::schema::buildings::{deleted_at, dsl::buildings, id},
};

/// Get all existing buildings in the database table and depending on the include_deleted argument also the soft deleted ones.
/// Returns a vector of buildings.
pub async fn get_all_buildings(pool: Pool, include_deleted: bool) -> Vec<Building> {
    let conn = pool.get().await.unwrap();
    if include_deleted {
        conn.interact(|conn| buildings.load::<Building>(conn).unwrap())
            .await
            .unwrap()
        // / include error-handling as this might fail
    } else {
        conn.interact(|conn| {
            buildings
                .filter(deleted_at.is_null())
                .load::<Building>(conn)
                .unwrap()
        })
        .await
        .unwrap()
    }
}

/// Find a building by UUID.
/// Returns a building with the corresponding UUID or `None` if the UUID does not exist in the table.
pub async fn find_building_by_id(pool: Pool, building_id: Uuid) -> Option<Building> {
    let conn = pool.get().await.unwrap();
    conn.interact(move |conn| buildings.find(building_id).first::<Building>(conn).ok())
        .await
        .unwrap()
}

/// Creates a new building using the building data transfer object passed as argument.
/// Returns the created building.
pub async fn create_building(pool: Pool, building: BuildingDTO) -> Building {
    let conn = pool.get().await.unwrap();

    conn.interact(|conn| {
        insert_into(buildings)
            .values(building)
            .get_result::<Building>(conn)
            .unwrap()
    })
    .await
    .unwrap()
}

/// Pass a building data transfer object as argument.
/// If the UUID already exists, update the building with the new name, address and deletion time.
/// If the UUID does not exist, create a new building with that UUID.
/// Returns the created or updated building.
pub async fn update_building(pool: Pool, building: BuildingDTO) -> Building {
    let conn = pool.get().await.unwrap();

    conn.interact(move |conn| {
        if let Ok(existing_building) = &buildings.find(building.id).get_result::<Building>(conn) {
            debug!("existing building: {:#?}", existing_building);
            debug!("building dto: {:#?}", building);
            update(existing_building)
                .set(&building)
                .get_result::<Building>(conn)
                .unwrap()
        } else {
            // could be replaced with unwrap_or(create_building(pool, building).await), but results
            // in closure returning a async block and requires move of ownership which leads to
            // problems with the lifetime of the connection object's mutable borrow
            insert_into(buildings)
                .values(building)
                .get_result::<Building>(conn)
                .unwrap()
        }
    })
    .await
    .unwrap()
}

/// Soft delete the building with the provided UUID.
/// Return true if deletion was successful and false if the UUID was not found.
pub async fn delete_building_by_id(pool: Pool, building_id: Uuid) -> bool {
    let conn = pool.get().await.unwrap();

    conn.interact(move |conn| {
        let not_deleted = select(exists(
            buildings
                .filter(id.eq(building_id))
                .filter(deleted_at.is_null()),
        ))
        .get_result(conn)
        .unwrap_or(false);

        if not_deleted {
            update(buildings.filter(id.eq(building_id)))
                .set(
                    deleted_at.eq(OffsetDateTime::now_local().unwrap_or(OffsetDateTime::now_utc())),
                )
                .execute(conn)
                .unwrap();
            return true;
        }
        return false;
    })
    .await
    .unwrap()
}
