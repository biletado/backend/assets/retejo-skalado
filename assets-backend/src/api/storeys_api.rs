use axum::{
    body::Body,
    extract::{Path, Query, State},
    http::{header, HeaderMap, Response, StatusCode},
    response::IntoResponse,
    Extension, Json,
};
use deadpool_diesel::postgres::Pool;
use serde::{Deserialize, Serialize};
use tracing::info;
use tracing_subscriber::fmt::format;
use uuid::Uuid;

use axum_keycloak_auth::decode::KeycloakToken;

use crate::api::common::{handle_error, NAME_REGEX};
use crate::database::crud::rooms_crud::has_rooms;
use crate::{
    api::auth::Role,
    database::{
        crud::storeys_crud,
        crud::storeys_crud::*,
        model::{Storey, StoreyDTO},
    },
};

#[derive(Deserialize)]
pub struct QueryParams {
    include_deleted: Option<bool>,
    building_id: Uuid,
}

#[derive(Serialize)]
pub struct Storeys {
    storeys: Vec<Storey>,
}

pub async fn get_storeys(
    State(pool): State<Pool>,
    Query(query_params): Query<QueryParams>,
) -> impl IntoResponse {
    info!(
        "operation={},objectType={},storeyId={}",
        "GET", "storey", query_params.building_id
    );

    let storeys_vec = get_storeys_by_building(
        pool,
        query_params.building_id,
        query_params.include_deleted.unwrap_or(false),
    )
    .await;

    Json(Storeys {
        storeys: storeys_vec,
    })
}

pub async fn add_storey(
    Extension(token): Extension<KeycloakToken<Role>>,
    State(pool): State<Pool>,
    Json(storey): Json<StoreyDTO>,
) -> impl IntoResponse {
    if !NAME_REGEX.is_match(&storey.name) {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid name".to_string(),
        ));
    };

    let created_storey = create_storey(pool, storey).await;
    info!(
        "operation={},objectType={},roomId={},userId={}",
        "POST", "storey", created_storey.id, token.subject
    );

    let mut headers = HeaderMap::new();
    let location_header = format!("/api/v2/assets/storeys/{}", created_storey.id);
    headers.insert(header::LOCATION, location_header.parse().unwrap());
    let mut response = (headers, Json(created_storey)).into_response();
    *response.status_mut() = StatusCode::CREATED;
    Ok(response)
}

pub async fn get_storey_by_id(
    State(pool): State<Pool>,
    Path(storey_id): Path<Uuid>,
) -> impl IntoResponse {
    info!(
        "operation={},objectType={},roomId={}",
        "GET", "storey", storey_id
    );
    let storey = find_storey_by_id(pool, storey_id).await;

    match storey {
        Some(storey) => Ok(Json(storey)),
        None => Err(handle_error(
            404,
            "not found".to_string(),
            "not found, but in more detail".to_string(),
        )),
    }
}

pub async fn update_storey(
    Extension(token): Extension<KeycloakToken<Role>>,
    State(pool): State<Pool>,
    Path(storey_id): Path<Uuid>,
    Json(mut storey): Json<StoreyDTO>,
) -> impl IntoResponse {
    if !NAME_REGEX.is_match(&storey.name) {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid name".to_string(),
        ));
    };
    if !storey.deleted_at.is_none() {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid deleted_at".to_string(),
        ));
    }

    info!(
        "operation={},objectType={},oldStoreyId={},storeyId={},userId={}",
        "PUT", "storey", storey.id, storey_id, token.subject
    );

    storey.id = storey_id;

    let mut headers = HeaderMap::new();
    let status_code = if !find_storey_by_id(pool.clone(), storey_id).await.is_some() {
        let location_header = format!("/api/v2/assets/storeys/{}", storey_id);
        headers.insert(header::LOCATION, location_header.parse().unwrap());
        StatusCode::CREATED
    } else {
        StatusCode::OK
    };

    let mut response = (
        headers,
        Json(storeys_crud::update_storey(pool, storey).await),
    )
        .into_response();
    *response.status_mut() = status_code;
    Ok(response)
}

pub async fn delete_storey(
    Extension(token): Extension<KeycloakToken<Role>>,
    State(pool): State<Pool>,
    Path(storey_id): Path<Uuid>,
) -> impl IntoResponse {
    if has_rooms(pool.clone(), storey_id).await {
        return Err(handle_error(
            StatusCode::BAD_REQUEST.as_u16(),
            StatusCode::BAD_REQUEST
                .canonical_reason()
                .unwrap()
                .to_owned(),
            "deletion not possible because of existing rooms".to_string(),
        ));
    }

    info!(
        "operation={},objectType={},deletedBuildingId={},userId={}",
        "PUT/DELETE", "storey", storey_id, token.subject
    );

    if !delete_storey_by_id(pool.clone(), storey_id).await {
        return Err(handle_error(
            StatusCode::NOT_FOUND.as_u16(),
            StatusCode::NOT_FOUND.canonical_reason().unwrap().to_owned(),
            "not found or already deleted".to_string(),
        ));
    }

    Ok(Response::builder()
        .status(StatusCode::NO_CONTENT)
        .body(Body::empty())
        .unwrap())
}
