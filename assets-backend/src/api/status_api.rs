use axum::extract::State;
use axum::response::IntoResponse;
use axum::Json;
use deadpool_diesel::postgres::Pool;
use serde_json::{json, Value};
use tracing::{error, info};

use reqwest;

pub async fn get_status() -> Json<Value> {
    info!("operation={},objectType={}", "GET", "status");
    let api_status = json!({
        "authors": [
            "LavaFly",
            "Calyx17",
        ],
        "apiVersion": "2.1.0",
    });
    Json(api_status)
}

pub async fn get_health(State(pool): State<Pool>) -> Json<Value> {
    info!("operation={},objectType={}", "GET", "health");

    let db_state: bool;
    match pool.get().await {
        Ok(conn) => db_state = true,
        Err(err) => db_state = false,
    }

    let health = json!({
        "live": true,
        "ready": true,
        "databases": {
            "assets": {
                "connected": db_state
            }
        }
    });
    Json(health)
}

pub async fn get_health_live() -> impl IntoResponse {
    info!("operation={},objectType={}", "GET", "health/live");

    let response = reqwest::get("http://localhost:6443/livez").await.unwrap();

    let answer;
    if response.status().is_success() {
        answer = json!({"live": true});
        Ok(Json(answer))
    } else {
        answer = json!({"live": false});
        Err((http::StatusCode::SERVICE_UNAVAILABLE, Json(answer)))
    }
}

pub async fn get_health_ready() -> impl IntoResponse {
    info!("operation={},objectType={}", "GET", "health/ready");

    let response = reqwest::get("http://localhost:6443/readyz").await.unwrap();

    let answer;
    if response.status().is_success() {
        answer = json!({"ready": true});
        Ok(Json(answer))
    } else {
        answer = json!({"ready": false});
        Err((http::StatusCode::SERVICE_UNAVAILABLE, Json(answer)))
    }
}
