use axum::{
    body::Body,
    extract::{Path, Query, State},
    http::{header, HeaderMap, Response, StatusCode},
    response::IntoResponse,
    Extension, Json,
};
use deadpool_diesel::postgres::Pool;
use serde::{Deserialize, Serialize};
use tracing::info;
use uuid::Uuid;

use axum_keycloak_auth::decode::KeycloakToken;

use crate::api::common::{handle_error, ADDRESS_REGEX, NAME_REGEX};
use crate::{
    api::auth::Role,
    database::{
        crud::buildings_crud,
        crud::buildings_crud::*,
        crud::storeys_crud::has_storeys,
        model::{Building, BuildingDTO},
    },
};

#[derive(Deserialize)]
pub struct QueryParams {
    include_deleted: Option<bool>,
}

#[derive(Serialize)]
pub struct Buildings {
    buildings: Vec<Building>,
}

pub async fn get_buildings(
    State(pool): State<Pool>,
    Query(query_params): Query<QueryParams>,
) -> impl IntoResponse {
    info!("operation={},objectType={}", "GET", "building");
    let buildings_vec =
        get_all_buildings(pool, query_params.include_deleted.unwrap_or(false)).await;
    Json(Buildings {
        buildings: buildings_vec,
    })
}

pub async fn add_building(
    Extension(token): Extension<KeycloakToken<Role>>,
    State(pool): State<Pool>,
    Json(building): Json<BuildingDTO>,
) -> impl IntoResponse {
    if !NAME_REGEX.is_match(&building.name) {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid name".to_string(),
        ));
    };
    if !ADDRESS_REGEX.is_match(&building.address) {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid address".to_string(),
        ));
    };

    let created_building = create_building(pool, building).await;
    info!(
        "operation={},objectType={},buildingId={},userId={}",
        "POST", "building", created_building.id, token.subject
    );

    let mut headers = HeaderMap::new();
    let location_header = format!("/api/v2/assets/buildings/{}", created_building.id);
    headers.insert(header::LOCATION, location_header.parse().unwrap());
    let mut response = (headers, Json(created_building)).into_response();
    *response.status_mut() = StatusCode::CREATED;
    Ok(response)
}

pub async fn get_building_by_id(
    State(pool): State<Pool>,
    Path(building_id): Path<Uuid>,
) -> impl IntoResponse {
    let building = find_building_by_id(pool, building_id).await;
    info!(
        "operation={},objectType={},buildingId={}",
        "GET", "building", building_id
    );

    match building {
        Some(building) => Ok(Json(building)),
        None => Err(handle_error(
            404,
            "not found".to_string(),
            "not found, but in more detail".to_string(),
        )),
    }
}

pub async fn update_building(
    Extension(token): Extension<KeycloakToken<Role>>,
    State(pool): State<Pool>,
    Path(building_id): Path<Uuid>,
    Json(mut building): Json<BuildingDTO>,
) -> impl IntoResponse {
    if !NAME_REGEX.is_match(&building.name) {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid name".to_string(),
        ));
    };
    if !ADDRESS_REGEX.is_match(&building.address) {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid address".to_string(),
        ));
    };
    if !building.deleted_at.is_none() {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid deleted_at".to_string(),
        ));
    }

    info!(
        "operation={},objectType={},oldBuildingId={},buildingId={},userId={}",
        "PUT", "building", building.id, building_id, token.subject
    );

    building.id = building_id;

    let mut headers = HeaderMap::new();
    let status_code = if !find_building_by_id(pool.clone(), building_id)
        .await
        .is_some()
    {
        let location_header = format!("/api/v2/assets/buildings/{}", building_id);
        headers.insert(header::LOCATION, location_header.parse().unwrap());
        StatusCode::CREATED
    } else {
        StatusCode::OK
    };

    let mut response = (
        headers,
        Json(buildings_crud::update_building(pool, building).await),
    )
        .into_response();
    *response.status_mut() = status_code;
    Ok(response)
}

pub async fn delete_building(
    Extension(token): Extension<KeycloakToken<Role>>,
    State(pool): State<Pool>,
    Path(building_id): Path<Uuid>,
) -> impl IntoResponse {
    if has_storeys(pool.clone(), building_id).await {
        return Err(handle_error(
            StatusCode::BAD_REQUEST.as_u16(),
            StatusCode::BAD_REQUEST
                .canonical_reason()
                .unwrap()
                .to_owned(),
            "deletion not possible because of existing storeys".to_string(),
        ));
    }

    info!(
        "operation={},objectType={},deletedBuildingId={},userId={}",
        "PUT/DELETE", "building", building_id, token.subject
    );

    if !delete_building_by_id(pool.clone(), building_id).await {
        return Err(handle_error(
            StatusCode::NOT_FOUND.as_u16(),
            StatusCode::NOT_FOUND.canonical_reason().unwrap().to_owned(),
            "not found or already deleted".to_string(),
        ));
    }

    Ok(Response::builder()
        .status(StatusCode::NO_CONTENT)
        .body(Body::empty())
        .unwrap())
}
