use axum::{
    body::Body,
    extract::{Path, Query, State},
    http::{header, HeaderMap, Response, StatusCode},
    response::IntoResponse,
    Extension, Json,
};
use axum_keycloak_auth::decode::KeycloakToken;
use deadpool_diesel::postgres::Pool;
use serde::{Deserialize, Serialize};
use tracing::info;
use uuid::Uuid;

use crate::api::common::{handle_error, NAME_REGEX};
use crate::{
    api::auth::Role,
    database::{
        crud::rooms_crud,
        crud::rooms_crud::*,
        model::{Room, RoomDTO},
    },
};

#[derive(Deserialize)]
pub struct QueryParams {
    include_deleted: Option<bool>,
    storey_id: Uuid,
}

#[derive(Serialize)]
pub struct Rooms {
    rooms: Vec<Room>,
}

pub async fn get_rooms(
    State(pool): State<Pool>,
    Query(query_params): Query<QueryParams>,
) -> impl IntoResponse {
    info!(
        "operation={},objectType={},storeyId={}",
        "GET", "room", query_params.storey_id
    );

    let rooms_vec = get_rooms_by_storey(
        pool,
        query_params.storey_id,
        query_params.include_deleted.unwrap_or(false),
    )
    .await;

    Json(Rooms { rooms: rooms_vec })
}

pub async fn add_room(
    Extension(token): Extension<KeycloakToken<Role>>,
    State(pool): State<Pool>,
    Json(room): Json<RoomDTO>,
) -> impl IntoResponse {
    if !NAME_REGEX.is_match(&room.name) {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid name".to_string(),
        ));
    };

    let created_room = create_room(pool, room).await;

    info!(
        "operation={},objectType={},roomId={},userId={}",
        "POST", "room", created_room.id, token.subject
    );

    let mut headers = HeaderMap::new();
    let location_header = format!("/api/v2/assets/rooms/{}", created_room.id);
    headers.insert(header::LOCATION, location_header.parse().unwrap());
    let mut response = (headers, Json(created_room)).into_response();
    *response.status_mut() = StatusCode::CREATED;
    Ok(response)
}

pub async fn get_room_by_id(
    State(pool): State<Pool>,
    Path(room_id): Path<Uuid>,
) -> impl IntoResponse {
    let room = find_room_by_id(pool, room_id).await;
    info!(
        "operation={},objectType={},roomId={}",
        "GET", "room", room_id
    );
    match room {
        Some(room) => Ok(Json(room)),
        None => Err(handle_error(
            404,
            "not found".to_string(),
            "not found, but in more detail".to_string(),
        )),
    }
}

pub async fn update_room(
    Extension(token): Extension<KeycloakToken<Role>>,
    State(pool): State<Pool>,
    Path(room_id): Path<Uuid>,
    Json(mut room): Json<RoomDTO>,
) -> impl IntoResponse {
    if !NAME_REGEX.is_match(&room.name) {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid name".to_string(),
        ));
    };
    if !room.deleted_at.is_none() {
        return Err(handle_error(
            400,
            "invalid input".to_string(),
            "invalid deleted_at".to_string(),
        ));
    }

    info!(
        "operation={},objectType={},oldRoomId={},roomId={},userId={}",
        "PUT", "room", room.id, room_id, token.subject
    );

    room.id = room_id;

    let mut headers = HeaderMap::new();
    let status_code = if !find_room_by_id(pool.clone(), room_id).await.is_some() {
        let location_header = format!("/api/v2/assets/rooms/{}", room.id);
        headers.insert(header::LOCATION, location_header.parse().unwrap());
        StatusCode::CREATED
    } else {
        StatusCode::OK
    };

    let mut response = (headers, Json(rooms_crud::update_room(pool, room).await)).into_response();
    *response.status_mut() = status_code;
    Ok(response)
}

pub async fn delete_room(
    Extension(token): Extension<KeycloakToken<Role>>,
    State(pool): State<Pool>,
    Path(room_id): Path<Uuid>,
) -> impl IntoResponse {
    info!(
        "operation={},objectType={},deletedBuildingId={},userId={}",
        "PUT/DELETE", "room", room_id, token.subject
    );

    if !delete_room_by_id(pool, room_id).await {
        return Err(handle_error(
            StatusCode::NOT_FOUND.as_u16(),
            StatusCode::NOT_FOUND.canonical_reason().unwrap().to_owned(),
            "not found or already deleted".to_string(),
        ));
    }

    Ok(Response::builder()
        .status(StatusCode::NO_CONTENT)
        .body(Body::empty())
        .unwrap())
}
