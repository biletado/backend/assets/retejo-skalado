use crate::api::route::init_router;
use ::axum_test::TestServer;

#[tokio::test]
async fn simple_test() {
    use super::super::*;
    dotenvy::dotenv().ok();

    // hehehe this is so ugly but good enough

    let app = init_router().await;

    let addr = "0.0.0.0:3000";
    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();

    let mut test_server = TestServer::new(app).unwrap();

    // testing the status-request
    let response = test_server.get("/api/v2/assets/status");

    // assert that the response is successful
    response.await.assert_status_ok();
}
