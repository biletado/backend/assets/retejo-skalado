use axum::{
    http::StatusCode,
    http::{header, HeaderValue, Method},
    response::IntoResponse,
    routing::get,
    routing::post,
    routing::put,
    Router,
};
use axum_keycloak_auth::{service::KeycloakAuthLayer, PassthroughMode};
use deadpool_diesel::postgres::Pool;
use std::sync::Arc;
use tower::ServiceBuilder;
use tower_http::{cors::CorsLayer, trace::TraceLayer};
use tracing::log::info;

use crate::{
    api::auth::{fetch_keycloak_public_key, Role},
    api::buildings_api::*,
    api::rooms_api::*,
    api::status_api::*,
    api::storeys_api::*,
    database::conn::init_connection_pool,
};

fn public_router() -> Router<Pool> {
    Router::new()
        // status routes
        .route("/api/v2/assets/status", get(get_status))
        .route("/api/v2/assets/health", get(get_health))
        .route("/api/v2/assets/health/live", get(get_health_live))
        .route("/api/v2/assets/health/ready", get(get_health_ready))
        // buildings routes
        .route("/api/v2/assets/buildings", get(get_buildings))
        .route("/api/v2/assets/buildings/:id", get(get_building_by_id))
        // storeys routes
        .route("/api/v2/assets/storeys", get(get_storeys))
        .route("/api/v2/assets/storeys/:id", get(get_storey_by_id))
        // rooms routes
        .route("/api/v2/assets/rooms", get(get_rooms))
        .route("/api/v2/assets/rooms/:id", get(get_room_by_id))
}

async fn protected_router() -> Router<Pool> {
    let pub_key = match fetch_keycloak_public_key().await {
        Some(pub_key) => pub_key,
        None => panic!("failed to fetch public key required for authentication"),
    };

    Router::new()
        // buildings routes
        .route("/api/v2/assets/buildings", post(add_building))
        .route(
            "/api/v2/assets/buildings/:id",
            put(update_building).delete(delete_building),
        )
        // storeys routes
        .route("/api/v2/assets/storeys", post(add_storey))
        .route(
            "/api/v2/assets/storeys/:id",
            put(update_storey).delete(delete_storey),
        )
        // rooms routes
        .route("/api/v2/assets/rooms", post(add_room))
        .route(
            "/api/v2/assets/rooms/:id",
            put(update_room).delete(delete_room),
        )
        // add auth layer
        .layer(
            KeycloakAuthLayer::<Role>::builder()
                .decoding_key(Arc::from(pub_key))
                .expected_audiences(vec![String::from("account")])
                // if changed to `PassthroughMode::Pass` function parameter has to be adapted to
                // `Extension(auth_status): Extension<KeycloakAuthStatus<Role>>`
                .passthrough_mode(PassthroughMode::Block)
                .build(),
        )
}

pub async fn init_router() -> Router {
    info!("Initializing Router");
    public_router()
        .merge(protected_router().await)
        // add cors layer
        .layer(
            CorsLayer::new()
                // allow `GET`, `POST`, `PUT` and `DELETE` HTTP request when accessing the resources
                .allow_methods([Method::GET, Method::POST, Method::PUT, Method::DELETE])
                // allow requests from localhost, alternatively set the origin parameter to any
                .allow_origin("http://localhost:9090".parse::<HeaderValue>().unwrap())
                // test if it is really necessary to explicitly add content-type to allow header
                .allow_headers([header::CONTENT_TYPE, header::AUTHORIZATION]),
        )
        // add tracing layer for HTTP requests and responses
        .layer(ServiceBuilder::new().layer(TraceLayer::new_for_http()))
        // add connection pool as state
        .with_state(init_connection_pool())
        // add a fallback service for handling routes to unknown paths
        .fallback(fallback_handler)
}

async fn fallback_handler() -> impl IntoResponse {
    (StatusCode::NOT_FOUND, "nothing to see here")
}
