use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use lazy_static::lazy_static;
use regex::Regex;
use serde::Serialize;
use std::fmt::Debug;
use uuid::Uuid;

#[derive(Serialize, Debug)]
pub struct Error {
    pub(crate) code: u16,
    pub(crate) message: String,
    pub(crate) more_info: String,
}

#[derive(Serialize, Debug)]
pub struct ErrorList {
    pub(crate) errors: Vec<Error>,

    // #[serde(default = "Uuid::new_v4")]
    pub(crate) trace: Uuid,
}

impl IntoResponse for ErrorList {
    fn into_response(self) -> Response {
        let status_code = self.errors.first().unwrap().code;
        (StatusCode::from_u16(status_code).unwrap(), Json(self)).into_response()
    }
}

pub fn handle_error(status_code: u16, message: String, more_info: String) -> Response {
    let error = ErrorList {
        errors: vec![Error {
            code: status_code,
            message,
            more_info,
        }],
        trace: Uuid::new_v4(),
    };
    return error.into_response();
}

lazy_static! {
    pub static ref ID_REGEX: Regex =
        Regex::new(r"^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$").unwrap();
    pub static ref NAME_REGEX: Regex =
        Regex::new(r"^([a-zA-ZäöüÄÖÜß0-9-.]+ )*([a-zA-ZäöüÄÖÜß0-9-.]+)$").unwrap();
    pub static ref ADDRESS_REGEX: Regex =
        Regex::new(r"^(([a-zA-ZäöüÄÖÜß0-9-.]+ )*([a-zA-ZäöüÄÖÜß0-9-.]*)\n?)*$").unwrap();
}
