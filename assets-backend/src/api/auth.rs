use jsonwebtoken::DecodingKey;
use reqwest::Client;
use serde::Deserialize;
use std::env;
use tracing::{error, info};

#[derive(Deserialize)]
struct KeycloakRealm {
    realm: String,
    public_key: String,
    #[serde(alias = "token-service")]
    token_service: String,
    #[serde(alias = "account-service")]
    account_service: String,
    #[serde(alias = "tokens-not-before")]
    tokens_not_before: i16,
}

/// Fetch the public RSA key for a realm existing on a Keycloak server.
pub async fn fetch_keycloak_public_key() -> Option<DecodingKey> {
    let keycloak_host = env::var("KEYCLOAK_HOST").unwrap();
    let keycloak_realm = env::var("KEYCLOAK_REALM").unwrap();
    let keycloak_url = format!("http://{}/auth/realms/{}", keycloak_host, keycloak_realm);

    let client = Client::new();
    let response = match client.get(keycloak_url).send().await {
        Ok(res) => res,
        Err(error) => {
            error!("Error while trying to fetch keycloak public key: {error}");
            return None;
        }
    };

    if response.status().is_success() {
        let keycloak_realm = response.json::<KeycloakRealm>().await.ok()?;
        let public_key = keycloak_realm.public_key.as_str();

        let pem_key = format!(
            "-----BEGIN PUBLIC KEY-----\n{}\n-----END PUBLIC KEY-----",
            public_key.trim()
        );
        info!("Received public key from keycloak");

        let decoding_key = DecodingKey::from_rsa_pem(pem_key.as_bytes()).ok()?;
        Some(decoding_key)
    } else {
        error!(
            "Error while trying to fetch keycloak public key: {}",
            response.status()
        );
        None
    }
}

// TBD: Define roles according to the realm settings in Keycloak
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Role {
    Administrator,
    Unknown(String),
}

impl axum_keycloak_auth::role::Role for Role {}

impl std::fmt::Display for Role {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Role::Administrator => f.write_str("Administrator"),
            Role::Unknown(unknown) => f.write_fmt(format_args!("Unknown role: {unknown}")),
        }
    }
}

impl From<String> for Role {
    fn from(value: String) -> Self {
        match value.as_ref() {
            "administrator" => Role::Administrator,
            _ => Role::Unknown(value),
        }
    }
}
