#!/bin/bash

IMAGE_REPOSITORY="registry.gitlab.com/biletado/backend/assets/retejo-skalado"
IMAGE_TAG="local-dev"

# build image using podman
podman build -f ./Dockerfile -t ${IMAGE_REPOSITORY}:${IMAGE_TAG}

# clean-up of remaining images <none>:<none>
podman rmi $(podman images --filter "dangling=true" -q --no-trunc)

# alternative: explicitly add unique tags to build images and remove only images with those tags
# add `LABEL stage=builder` to the build image in Containerfile
# run `podman image prune --filter label=stage=builder`

# export and push image to kind cluster
# tar export can be omitted when using `kind load docker-image ${IMAGE_REPOSITORY}:${IMAGE_TAG} --name kind-cluster`
# using workaround because the above command is only capable of handling docker images - as the name already suggests
# shorter solution: podman save image ${IMAGE_REPOSITORY}:${IMAGE_TAG} | kind load image-archive -
podman save ${IMAGE_REPOSITORY}:${IMAGE_TAG} --format oci-archive -o assets-backend.tar
kind load image-archive assets-backend.tar -n kind-cluster

# clean-up working directory
rm assets-backend.tar

# patch kind-cluster using kustomization.yaml and wait for pods ready state
kubectl apply -k . --prune -l app.kubernetes.io/part-of=biletado -n biletado
kubectl rollout restart deployment assets -n biletado
kubectl wait pods -n biletado -l app.kubernetes.io/part-of=biletado --for condition=Ready --timeout=120s
