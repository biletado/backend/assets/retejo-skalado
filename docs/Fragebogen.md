# Fragebogen zum Programmentwurf Webengineering 2 DHBW Karlsruhe TINF21B3

## Gruppeninformationen

Gruppenname: retejo skalado

Gruppenteilnehmer:

- Calyx17
- LavaFly

## Quellcode

Links zu den Versionskontrollendpunkten:

- https://gitlab.com/web-engineering-ii/biletado-backend.git

## Lizenz

- assets-backend
  - MIT: License
- axum-keycloak-auth
  - MIT: LICENSE-MIT
  - APACHE: LICENSE-APACHE

## Sprache und Framework

| Frage                                 | Beispiel                                           | Antwort                                                                                                        |
|---------------------------------------|----------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
| Programmiersprache                    | go                                                 | rust                                                                                                           |
| Sprachversion                         | 1.17                                               | 1.73.0                                                                                                         |
| Version ist aktuell und wird gepflegt | [X]                                                | [x]                                                                                                            |
| Framework (FW)                        | "gin-gonic" oder "stdlib"                          | axum                                                                                                           |
| FW-Version                            | v1.8.1                                             | 0.7.1                                                                                                          |
| FW-Version ist aktuell                | [X]                                                | [x]                                                                                                            |
| Website zum FW                        | [gin-gonic](https://gin-gonic.com)                 | [GitHub-Repository](https://github.com/tokio-rs/axum), [Crate-Dokumentation](https://docs.rs/axum/latest/axum) |
| Prepared statements/ORM               | "doctrine", "FW-Integriert", "prepared statements" | diesel                                                                                                         |
| ORM Version                           | 2.13                                               | 2.1.4                                                                                                          |
| ORM Version ist aktuell               | [X]                                                | [x]                                                                                                            |
| Website zum ORM                       | [doctrine-orm](https://www.doctrine-project.org/)  | [Website](https://diesel.rs), [GitHub-Repository](https://github.com/diesel-rs/diesel)                         |

## Automatisierung

Art der Automatisierung:
- Continuous Integration und Continuous Delivery/Continuous Deployment (CI/CD): GitLab CI/CD-Pipeline `.gitlab-ci.yml`
- lokale Entwicklungsumgebung: `podman-build-kind-deploy.sh`

## Testautomatisierung

Art der Testautomatisierung:
- Scannen des zuvor in der Build-Stage gebauten und in der Container-Registry abgelegten Containers auf Schwachstellen
- Scannen des Repositorys auf versehentlich hinzugefügte Secrets wie API Tokens oder private Schlüssel

Wie sind die Ergebnisse einzusehen?

Bei erfolgreicher Ausführung der Jobs `container_scanning` und `secret_detection` befinden sich die Resultate als JSON-Dateien in den jeweiligen Job-Artefakten auf GitLab.

Die Anwendung lässt sich durch den Befehl `cargo test` testen. Die Ergebnisse werden auf `stdout` ausgegeben.

## Authentifizierung

* JWT wird berücksichtigt: [x]
* Signatur wird geprüft: [x]

## Konfiguration und Dokumentation

* Dokumentation existiert in bedarfsgerechtem Umfang: [x]
* Konfigurationsparameter sind sinnvoll gewählt: [x]
* keine hardcoded Zugänge zu angeschlossenen Systemen (URLs, Passwörter, Datenbanknamen, etc.): [x]
* Umgebungsvariablen und Konfigurationsdateien sind gelistet und beschrieben: [x]

## Logging
* Log-System des Frameworks oder Bibliothek wurde genutzt: [x]
* Logs enthalten alle geforderten Werte: [x]
* LogLevel ist konfigurierbar: [x]
