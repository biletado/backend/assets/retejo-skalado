# TODO

## Build and deploy automation
- [x] add multi-stage containerfile with build and runtime images (image size: runtime << build)
- [x] improve runtime image size using distroless images from Google
- [x] add script or makefile for building image and deploying it to the kind cluster (local development)
- [x] add configuration file for GitLab CI/CD pipeline

## API implementation
- [x] choose web framework (axum, actix, warp, rocket)
- [x] choose ORM (diesel, rbatis, seaorm) or sql client abstraction framework (sqlx-postgres, tokio-postgres)
- [x] include CORS in order to send HTTP request from localhost to pod in kind cluster 
- [ ] implement specification of assets API:
  - [ ] error handling 
    - [ ] for query parameters -> custom error handling for extractors used in function parameters
    - [ ] for API calls -> include HTTP responses 4XX, 5XX matching specification → see https://docs.rs/axum/latest/axum/error_handling/index.html#axums-error-handling-model
    - [ ] use anyhow error? -> see for example https://github.com/pbzweihander/axum-anyhow-example/blob/main/src/main.rs, https://antoinerr.github.io/blog-website/2023/01/28/rust-anyhow.html
    - [ ] improve quick fix for async function calls returning Future<...> with e.g. map_error and ? notation
    - [ ] for unwrap() -> panics if not handled (e.g. work with map_error() or unwrap_or() depending on context)
    - [ ] for database connection
  - [x] input validation for POST und PUT HTTP requests (e.g. no \n in name) according to specification → use provided regex
  - [x] deleted_at may only be set to null in put
  - [ ] location in response header for post and put
  - [ ] handle inserting storey/room with faulty building/storey id -> DatabaseError(ForeignKeyViolation)
  - [x] change "The JWT could not be decoded. Source: ExpiredSignature" to expired error
  - [ ] check if all requirements from the specification are matched -> test special cases using rapidoc
  - [x] add requests to kind API in order to receive status information and include it in status api
  - [x] implement from/into function for conversion between DTOs und actual database representation structs
  - [x] own struct for create, update etc. in order to match specification -> currently possible to set values that should not be allowed to set

 - [x] change data type of timestamp to match specification:
    - change database type of `deleted_at` from timestamp to postgres specific type timestampz and use OffsetDateTime instead of PrimitiveDateTime → solve through applying patches in kustomaziation.yaml? 
    - replace time with chrono NaiveDateTime and use `#[with=...]` to set serializing function, if it does not already default to the specified format or implement into response with conversion to DateTimeOffset and rfc3339 function call
    - apply migrations and include them in binary? -> see https://github.com/tokio-rs/axum/blob/axum-0.6.21/examples/diesel-postgres/src/main.rs
- [x] if error also appears on different machines: search for error leading to not reproducible return code 3 when getting connection from lazy-static connection pool or replace with deadpool_diesel
- [x] handle missing deleted_at in query string parameters
- [x] exclude deleted_at from json response if null
- [x] directly insert dtos: add required macros, set default values for (de)serializing with serde
- [x] adapt to new example data (missing address) -> perhaps not intended
- [ ] evaluate advantages of diesel async and bb8 as connection pool

### Refactoring: Architecture & Code Quality
- [ ] divide package into lib and bin crate for better reusability and cleaner architecture
- [ ] make return types of api functions more specific -> replace impl IntoResponse with concrete type
- [ ] replace sql operations built with diesel query builder with orm pendants -> map errors directly instead of in API code?
- [ ] refactoring of *_api und *_crud to avoid code duplication → maybe ask Lindner for help/clarification in the next lecture
- [ ] map relations in postgres database to rust structs -> read diesel docs

## Authorization
- [x] evaluate concept for user authorization using jwt (middleware crates, possible implementations) → see https://github.com/tokio-rs/axum/blob/axum-0.6.21/examples/jwt/src/main.rs
- [x] pure Rust implementation or authorization through further pod in kind cluster?
- [ ] jwt auth: decide between pass and block, include in function signature, find solution to adapt crate error responses to match specification (401 and error list) -> modify crate code and include in local build using cargo workspaces (requirements concerning keycloak could be changed) or add error handling in API function
  remark: unfortunately trait implementation cannot be overriden in Rust -> wrapper
- [ ] set audience field as environment variable

## Testing
- [x] container image scanning & secret detection in `.gitlab-ci.yml`
- [x] implement unit test and include execution with `cargo test` in test stage of `.gitlab-ci.yml`

## Logging
- [x] check requirements regarding logging structure and values
  - [x] self-generated UUID `tracing` in HTTP error response: include in log messages as well as responses
- [x] choose logging crate
- [x] include initialization in `main.rs` (keep the environment variable indicating the log level in mind)
- [ ] use diesel_tracing or diesel-logger for logging database access
- [x] add logging messages to source code, respectively replace simple prints to stdout
- [ ] add pod to kind cluster used for persisting logs written to stdout
- [ ] tracing with traefik?

## Kustomization
- [ ] apply further patches matching the changes and additions: 
  - [x] environmental variable for Rust log level (RUST_LOG_LEVEL)
  - [ ] include changes of Keycloak configuration made via web interface -> change file https://gitlab.com/biletado/kustomize/-/blob/main/base/iam/keycloak/realm-export.json?ref_type=heads or use sql database migrations
  - [x] perhaps: change data type of `deleted_at` from timestamp to timestamptz -> change file https://gitlab.com/biletado/kustomize/-/blob/main/base/database/postgres/kustomization.yaml?ref_type=heads or use sql database migrations

## Documentation
- [x] add table about environment variables to readme
- [x] add section about project structure and logging concept
- [x] add section regarding local development software requirements (postgres-driver) and their environment variables
- [x] add section about keycloak requirements (needed fields, expiration), audience and role
- [x] update readme concerning cargo workspace with lib crate axum-keycloak-auth: why? what changes habe been made? -> make clear that it is a fork
- [ ] fill out the remaining parts of the questionnaire

## General
- [x] add license to repository
- [ ] add Matthias with maintainer? access rights to repository in order to make CI/CD accessible or make repository public?

## Questions
- timestamp format: rfc3339 or just human-readable format required
- tracing/logging of complete traffic going to assets pod -> traefik with jaeger
- persisting logs when pod is exited?
- UUID for in error list: self-generated UUID `tracing` in HTTP error response: include in log messages as well as responses? Generally speaking, what is the purpose of that UUID?
- Keycloak JWT token expiration time, login using Keycloak, audience always account?
- duplication of already existing objects, soft delete object during creation -> e.g. add already existing buildings
