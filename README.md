# assets-backend for biletado

## Table of contents
<!-- TOC -->
* [assets-backend for biletado](#assets-backend-for-biletado)
  * [Table of contents](#table-of-contents)
  * [Development on local machine](#development-on-local-machine)
    * [Building the project locally](#building-the-project-locally)
    * [Building an image for local development using Podman](#building-an-image-for-local-development-using-podman)
    * [`podman-build-kind-deploy.sh`](#podman-build-kind-deploysh)
  * [Using self-hosted GitLab Runners as Podman containers](#using-self-hosted-gitlab-runners-as-podman-containers)
  * [Project Structure](#project-structure)
    * [Database Module (`database` with submodule `crud`)](#database-module-database-with-submodule-crud)
    * [API Module (`api`)](#api-module-api)
    * [Authorization using JWT (JSON Web Token)](#authorization-using-jwt-json-web-token)
      * [Crate Requirements for Keycloak](#crate-requirements-for-keycloak)
  * [CI/CD Pipeline](#cicd-pipeline)
    * [Containerfile](#containerfile)
  * [Logging](#logging)
  * [Environment variables](#environment-variables)
<!-- TOC -->

## Development on local machine
### Building the project locally
Building the project requires the following dependencies:
- Rust
- PostgreSQL
  - Linux: `libpq-dev`
  - Windows: PostgreSQL CLI, environment variables for `PQ_LIB_DIR`, `bin` and `lib` folder in `PATH`
- OpenSSL
  - Linux: `pkg-config` and `libssl-dev`

In order to build and run the project using `cargo` build tools, execute the following commands:
~~~
cargo build  # fetch dependencies and build the project to the output folder ./target
cargo run    # run the project using ./.env as environment
~~~

### Building an image for local development using Podman
~~~ bash
podman build -f ./Dockerfile -t registry.gitlab.com/biletado/assets-backend:local-dev
~~~

### `podman-build-kind-deploy.sh`
The bash script fulfills the following tasks:
- build Podman image using multi-stage `containerfile` 
- push the runtime image to the kind cluster
- clean-up of the working directory and remaining Podman images
- patch kind cluster using `kustomization.yaml`

## Using self-hosted GitLab Runners as Podman containers
> When creating the GitLab Runner using the project settings in the web interface of GitLab you either have to allow
> the runner to take untagged jobs, or you provide tags that must match the corresponding entries in the 
> `.gitlab-ci.yml`. It is highly recommended to use tags.

Start the actual runner and pass the Podman socket.
~~~ bash
podman run -d `
    --name="gitlab-runner" `
    -v /var/run/podman:/var/run/podman `
    -v gitlab-runner-config:/etc/gitlab-runner `
    registry.gitlab.com/gitlab-org/gitlab-runner:latest
~~~

In order to register the runner for the project, navigate to Settings → CI/CD Settings → Runners and create a new 
project runner. Replace `<token>` with the generated runner token.
~~~ bash
podman run --rm -it \
    -v gitlab-runner-config:/etc/gitlab-runner \
    registry.gitlab.com/gitlab-org/gitlab-runner:latest \
    register \
        --token="<TOKEN>" \
        --url https://gitlab.com/ \
        --non-interactive \
        --docker-host="unix:///var/run/podman/podman.sock" \
        --docker-privileged \
        --executor="docker" \
        --docker-image="alpine:latest" \
~~~

## Project Structure
The Rust binary crate consists of two modules:
- `database` module offers an interface for interacting with the postgres database
- `api` module implements handler functions for the registered routes and provides access control to API resources 
for all HTTP requests using JWT authorization

### Database Module (`database` with submodule `crud`)
The database connectivity is implemented using `diesel`, a Rust ORM framework for PostgreSQL.
Connections to the database are handled with `deadpool`, which allows creation of a thread pool to handle incoming
requests, so that connections are not created and destroyed on every request.

The CRUD functionality relating to the assets API is implemented in the submodule `crud`.
The functionality is split by the object type (building, storey, and room).

### API Module (`api`)
The `api` module encompasses the business logic handling API requests, and in this case, it leverages the Axum web 
framework for Rust. Axum allows for the incorporation of middleware modules to enhance functionality. 
The implementations of the CRUD API for buildings, storeys, and rooms are organized within their respective module files.

### Authorization using JWT (JSON Web Token)
Authorization is implemented using a middleware service for axum called `axum-keycloak-auth` with custom error
responses in order to match the API specification.

#### Crate Requirements for Keycloak
The crate `axum-keycloak-auth` requires the JWT to contain several mandatory fields.
Taking the [standard claim](https://github.com/lpotthast/axum-keycloak-auth/blob/main/src/decode.rs#L56) into
consideration, the following fields must be set in the settings of the Keycloak user who generated the JWT:
- first name
- last name
- preferred username
- email address

Furthermore, the audience field included in the JWT must be `account`.
See [this link](https://dev.to/metacosmos/how-to-configure-audience-in-keycloak-kp4) for further information on
audiences in Keycloak.

> In order to bypass these requirements that are by default not fulfilled by the provided test data in the PostgreSQL 
> database and fix compatibility issues that occurred when updating the axum dependency to version 0.7, we decided to
> use `cargo workspace` instead of a single binary crate and integrate a modified version of `axum-keycloak-auth` as a 
> library crate. Therefore, implementing custom error responses is easier and the obligatory fields can be changed to
> optional fields, so that no manipulation of the test data using SQL or further patches of the *.yaml files is required.

## CI/CD Pipeline
This projects uses GitLab CI as a continuous integration / continuous delivery system.
The pipeline derived from [Biletado/Pipeline](https://gitlab.com/biletado/pipeline) compiles the Rust binary crate 
using the provided containerfile and pushes a container image to the GitLab container registry of the repository.
In test stage the pipeline also runs container scanning, secret detection and unit tests and fails if these do not pass.

### Containerfile
The multi-stage containerfile uses the official `rust:slim` image for building purposes. This building image contains 
added dependencies like the PostgreSQL driver libpq-dev and the OpenSSL library libssl-dev. The resulting build artefact
of the Rust application is then copied to the `gcr.io/distroless/cc-debian12` distroless image, where it finally will be 
executed. In order to work properly with PostgreSQL database service, the PostgreSQL driver libraries must be included 
in the runtime image. As a result, the total size of the runtime image is reduced to around 60 MB, compared to 850 MB 
base image size of a `rust:slim`.

## Logging
The application implemented in Rust uses [`tracing_subscriber`](https://docs.rs/tracing-subscriber/0.3.18/tracing_subscriber/index.html) 
as logging framework. The log level can be defined using the environmental variable `RUST_LOG_LEVEL`.
The logs are written to `stdout` in JSON format. Allowed log levels are `debug`,`info`,`warning`,`error`.

## Environment variables
The application uses following environment variables for configuration purposes (for example usage see `.env`):

| Variable                 | Description                                                                                 |
|--------------------------|---------------------------------------------------------------------------------------------|
| KEYCLOAK_HOST            | host address of the Keycloak authentication server                                          |
| KEYCLOAK_REALM           | Keycloak realm that supplies the public key for JWT authentication                          |
| POSTGRES_ASSETS_USER     | username of the PostgreSQL database server                                                  |
| POSTGRES_ASSETS_PASSWORD | password of the PostgreSQL database server                                                  |
| POSTGRES_ASSETS_DBNAME   | assets database name on the PostgreSQL database server                                      |
| POSTGRES_ASSETS_HOST     | host address of the PostgreSQL database server                                              |
| POSTGRES_ASSETS_PORT     | port for accessing the PostgreSQL service                                                   |
| RUST_LOG_LEVEL           | set the log level of the rust application (possible values: error, warn, info, debug, trace |
